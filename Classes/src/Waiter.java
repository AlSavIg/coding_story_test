public class Waiter extends Person{
    private final int time;
    public Waiter(String name, int age, int time) {
        super(name, age);
        this.time = time;
    }

    @Override
    public int getAge() {
        return super.getAge();
    }
    @Override
    public String getName() {
        return super.getName();
    }
    public int getTime() {
        return time;
    }
}
